﻿using System;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace XFTableTest
{
   public partial class App : Application
   {
      public App()
      {
         InitializeComponent();

         MainPage = new LongListPage();
      }

      protected override void OnStart()
      {
         // Handle when your app starts
      }

      protected override void OnSleep()
      {
         // Handle when your app sleeps
      }

      protected override void OnResume()
      {
         // Handle when your app resumes
      }

      public static void print(string text)
      {
         Console.WriteLine(text);
      }

      /// Runs async code on UI thread. Invoke as follows:
      ///    await App.RunOnUiThreadAsync(async () => { await some_other_task; }); 
      public static Task RunOnUiThread(Func<Task> action)
      {
         var taskCompletion = new TaskCompletionSource<object>();
         Device.BeginInvokeOnMainThread(async () =>
         {
            try
            {
               await action();
               taskCompletion.SetResult(null);
            }
            catch (Exception exc)
            {
#if DEBUG
               System.Diagnostics.Debug.WriteLine(exc.ToString());
#endif
               taskCompletion.SetException(exc);
            }
         });
         return taskCompletion.Task;
      }


   }
}
