﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace XFTableTest
{
   // Learn more about making custom code visible in the Xamarin.Forms previewer
   // by visiting https://aka.ms/xamarinforms-previewer
   [DesignTimeVisible(false)]
   public partial class MainPage : ContentPage
   {




      public MainPage()
      {
         InitializeComponent();

         rTestTable.Root = new TableRoot
         {
            new TableSection("Ring")
            {
                // TableSection constructor takes title as an optional parameter
                new SwitchCell { Text = "New Voice Mail" },
                new SwitchCell { Text = "New Mail", On = true }
            },
            new TableSection("Entries")
            {
               new EntryCell { Label="Email Address", Placeholder="Required", HorizontalTextAlignment=TextAlignment.Center },
               new EntryCell { Label="First Name", Placeholder="Required", HorizontalTextAlignment=TextAlignment.End },
               new EntryCell { Label="Middle Name", Placeholder="Required" , HorizontalTextAlignment=TextAlignment.End},
               new EntryCell { Label="Last Name", Placeholder="Required" , HorizontalTextAlignment=TextAlignment.End},
               new EntryCell { Label="Entry1", Placeholder="Required", HorizontalTextAlignment=TextAlignment.End },
               new EntryCell { Label="Entry2", Placeholder="Required" , HorizontalTextAlignment=TextAlignment.End},
               new EntryCell { Label="Entry3", Placeholder="Required", HorizontalTextAlignment=TextAlignment.End },
               new EntryCell { Label="Entry4", Placeholder="Required", HorizontalTextAlignment=TextAlignment.End },
            },
            new TableSection("Person")
            {
               new ViewCell { View = new Grid {
                  ColumnDefinitions = { new ColumnDefinition { Width = new GridLength(1, GridUnitType.Star) },new ColumnDefinition{Width= new GridLength(2,GridUnitType.Star) } },
                  Children = { {new Label{ Text="Person 1"},0,0 }, {new Entry{Placeholder="required", },1,0 }
               } } },
               new ViewCell { View = new Grid {
                  ColumnDefinitions = { new ColumnDefinition { Width = new GridLength(1, GridUnitType.Star) },new ColumnDefinition{Width= new GridLength(2,GridUnitType.Star) } },
                  Children = { {new Label{ Text="Person XXX"},0,0 }, {new Entry{Placeholder="required", },1,0 }
               } } },
            }
         };
         rTestTable.Intent = TableIntent.Settings;
      }
   }
}
