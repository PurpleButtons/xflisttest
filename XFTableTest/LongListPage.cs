﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;
using Xamarin.Forms;

namespace XFTableTest
{

   public class RowData
   {
      public string Caption { get; set; }
      public string IdLabel { get; set; }

      public override string ToString()
      {
         return Caption;
      }
   }


   public class LongListPage : ContentPage
   {

      const string kRootUrl = "https://jsonplaceholder.typicode.com/";


      ObservableCollection<RowData> items;

      ListView listView;

      static HttpClient httpClient = new HttpClient();

      public LongListPage()
      {
         items = new ObservableCollection<RowData>();
         for (var i = 0; i < 30; i++)
         {
            items.Add(new RowData { Caption = $"Row {i}" });
         }

         listView = new ListView
         {
            BackgroundColor = Color.Yellow,
            ItemsSource = items,

         };
         listView.ItemAppearing += ListView_ItemAppearing;
         Content = listView;

         //grab an extra 25 rows
         Device.BeginInvokeOnMainThread(async () => {
            await FetchRows(25, 0);
         });

      }

      private async void ListView_ItemAppearing(object sender, ItemVisibilityEventArgs e)
      {
         if (e.ItemIndex > (items.Count - 10))
         {
            await FetchRows(25, 0);
            App.print("got rows: " + items.Count);
         }
      }


      //used to ensure we only make one fetch of rows
      //when we get to the bottom of the list
      TaskCompletionSource<object> fFetchRowsTask;


      // called to pull rows from the server when needed
      // will add them to our list when it gets them
      // if already waiting, then just wait for the previous call to complete
      async Task FetchRows(int pageLength, int startIndex)
      {
         var existingTask = fFetchRowsTask?.Task;
         if (existingTask != null)
         {
            await existingTask;
            return;
         }
         fFetchRowsTask = new TaskCompletionSource<object>();
         App.print("waiting to download new rows");
         await App.RunOnUiThread(async () =>
         {
            string json = await httpClient.GetStringAsync($"{kRootUrl}posts");   //needs a try/catch wrapper
            var jsonArray = JArray.Parse(json);
            for (var i = 0; i < pageLength; i++)
            {
               var ix = startIndex + i;
               if (ix >= jsonArray.Count)
               {
                  break;
               }
               var entry = jsonArray[ix];
               //               var index = int.Parse(entry["id"].ToString());
               var text = $"{items.Count}-> {entry["title"]}";
               items.Add(new RowData { Caption = text });
            }
            App.print("got rows");
            // follow safe order of operations to notify awaiters
            var safeCopyOfTaskCompletion = fFetchRowsTask;
            fFetchRowsTask = null;
            safeCopyOfTaskCompletion.SetResult(null);
         });
      }
   }
}

